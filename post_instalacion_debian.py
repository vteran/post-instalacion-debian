#! -*- coding:utf-8 -*-
import subprocess
import os
from subprocess import STDOUT

"""

This program make a Install of some software as Post Install of Debian(R)
wheezy 7.0

Copyright (C) 2013  Ing. Victor Teran H
    elalcon89 @nospam @hotmail.com


This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

"""

DISCLAIMER="""
    PostInstalacion Debian version 0.01, Copyright (C) 2013 Ing. Victor Teran H
    elalcon89@nospam@hotmail.com
    PostInstalacion Debian  comes with ABSOLUTELY NO WARRANTY;
    GPL V2
"""


PAQUETES_MULTIMEDIA="libdvdcss2 flashplugin-nonfree apt faad gstreamer0.10-ffmpeg gstreamer0.10-x gstreamer0.10-fluendo-mp3 gstreamer0.10-plugins-base  gstreamer0.10-plugins-good gstreamer0.10-plugins-bad gstreamer0.10-plugins-ugly ffmpeg lame twolame vorbis-tools libquicktime2 libfaac0 libmp3lame0 libxine1-all-plugins libxine2-all-plugins-libdvdread4 libdvdnav4 libmad0 libavutil51 sox libxvidcore4 libavcodec53 libavcodec54 libavdevice53 libavdevice54 libstdc++5"

PAQUETES_DESARROLLO="git mysql-server mysql-client mysql-common php5 libapache2-mod-php5 g++ python-dev python-setuptools"

PAQUETES_COMPRESION="rar unrar zip unzip unace bzip2 lzop p7zip-full p7zip-rar"

PAQUETES_LIBREOFFICE="libreoffice-l10n-es openclipart-libreoffice iceweasel-l10n-es-mx"

def instalar(paquete):
    print "PREPARANDOME PARA INSTALAR %s" % paquete
    #proc = subprocess.Popen('aptitude install -y FILE', shell=True, stdin=None, stdout=open(os.devnull,"wb"), stderr=STDOUT, executable="/bin/bash")
    os.system("apt-get install -y "+ paquete)
    #import pdb; pdb.set_trace()
    #proc.wait()

def instalacion(PAQUETES,modo=1):
    #import pdb; pdb.set_trace()
    for paquete in PAQUETES.split():
        instalar(paquete)
    if modo:
        while(True):
            opcion=raw_input("Instalación de codecs privativos: Elija su version de Sistema Operativo: 32 o 64")
            if opcion=="32":
                instalar("w32codecs")
                break
            elif opcion=="64":
                instalar("w64codecs")
                break
            else:
                print "Opcion invalida"
        
def Principal():
    print DISCLAIMER
    modos={"Codecs Privativos":PAQUETES_MULTIMEDIA, \
    "Herramientas Desarrollo":PAQUETES_DESARROLLO, \
    "Compresion":PAQUETES_COMPRESION, "Libre Office":PAQUETES_LIBREOFFICE}
    for i in range(len(modos.keys())):
        res=raw_input("Desea Instalar %s : Si No: \n" % modos.keys()[i])
        if modos.keys()[i]=="Codecs Privativos":
            bandera=True
        else:
            bandera=False
        if res=="Si" or res=="si":
            instalacion(modos.values()[i], bandera)
        elif res=="No" or res=="no":
            print "Abortado %s" % (modos.values()[i])
if __name__=="__main__":
    try:
        Principal()
    except KeyboardInterrupt:
        print "\n Accion Finalizada por el usuario \n "
        
